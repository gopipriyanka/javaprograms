package com.arr;

import java.util.Scanner;

public class Student {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of students:");
        int numStudents = scanner.nextInt();
        scanner.nextLine(); // Consume the newline character


        for (int i = 1; i <= numStudents; i++) {
            System.out.println("Enter Student Name:");
            String sname = scanner.nextLine();
            int[] marks = new int[3];

            System.out.println("Enter Marks for Subject 1:");
            marks[0] = scanner.nextInt();
            System.out.println("Enter Marks for Subject 2:");
            marks[1] = scanner.nextInt();
            System.out.println("Enter Marks for Subject 3:");
            marks[2] = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character

            
            int total = marks[0] + marks[1] + marks[2];
            double average = total / 3.0;

            String division = "";
            if (average >= 60) {
                division = "I Division";
            } else if (average >= 50) {
                division = "II Division";
            } else if (average >= 40) {
                division = "III Division";
            } else {
                division = "Fail";
            }

            // Generate Student ID automatically
            int sid = 100 + i;

            // Print the result
            System.out.println("Sid\tSname\tS1\tS2\tS3\tTot\tAvg\tResult");
            System.out.println(sid + "\t" + sname + "\t" + marks[0] + "\t" + marks[1] + "\t" + marks[2] + "\t" +
                    total + "\t" + String.format("%.2f", average) + "\t" + division );
        }

        scanner.close();
    }
}
