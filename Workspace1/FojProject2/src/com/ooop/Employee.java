package com.ooop;

public class Employee {

	int id;
	String name;
	int salary;
	
	public Employee(int id,String name,int salary){
		this.id = id;
		this.name = name;
		this.salary=salary;
	}
	public void details(){
	System.out.println("Employee ID : " + id);	
	System.out.println("Employee Name : " + name);
	System.out.println("Employee Salary:"+salary);
	}
	

	public static void main(String[] args) {
		Employee e = new Employee(21,"Gopi Priyanka",50000);
		e.details();
	}
}
