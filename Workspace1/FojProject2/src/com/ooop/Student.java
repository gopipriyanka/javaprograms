package com.ooop;

public class Student {

	int id;
	String name;
	
	public Student(int id,String name){
		this.id = id;
		this.name = name;
	}
	public void details(){
	System.out.println("Student ID : " + id);	
	System.out.println("Student Name : " + name);	
	}
	

	public static void main(String[] args) {
		Student student = new Student(21,"Gopi Priyanka");
		student.details();
	}


}
