package com.demo1;

public class sum_of_threedigits {
	public static int sum(int num) {
		int firstDigit = num / 100;  // Extract the first digit
		int secondDigit = (num / 10) % 10;  // Extract the second digit
		int thirdDigit = num % 10;  // Extract the third digit

		return firstDigit + secondDigit + thirdDigit;  // Return the sum of digits
	}

	public static void main(String[] args) {
		int result = sum(235);  // Example three-digit number
		System.out.println("sum = " + result);
	}
}
