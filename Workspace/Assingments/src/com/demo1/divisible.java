package com.demo1;

public class divisible {

//	public static int divisibility(int num) {
//		if(num % 3 == 0) {
//			System.out.println("fizz!!");
//		}else if(num % 5 == 0) {
//			System.out.println("buzz!!");
//		}else if(num % 3 == 0 && num % 5 == 0) {
//			System.out.println("it is fizz & buzz");
//		}else {
//			System.out.println("invalid");
//		}
//		return result;
//	}
//
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//
//		System.out.println(divisibility(30));
//		System.out.println(divisibility(20));
//		System.out.println(divisibility(15));
//		System.out.println(divisibility(4));
//	}
	
	public static String fizzBizz(int num){
		String result = "";
		
		if (num % 3 == 0)
			result += "Fizz";
		
		if (num % 5 == 0)
			result += "Bizz";
		
		return result;
	}

	public static void main(String[] args) {
		System.out.println(fizzBizz(3));
		System.out.println(fizzBizz(5));
		System.out.println(fizzBizz(15));

	}

}
