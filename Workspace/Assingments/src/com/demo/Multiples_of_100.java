package com.demo;

public class Multiples_of_100 {
	public static int nextmultiple100(int number) {    
        int difference = 100 - (number % 100);
        int nextMultiple = number + difference;

        System.out.println("The next multiple of 100 after " + number + " is = " + nextMultiple);

        return nextMultiple;
    }

    public static void main(String[] args) {
        nextmultiple100(25);
        nextmultiple100(100);
    }
}
