package com.demo2;

public class SpeedCalculator {
	public static int speed() {
		return 0;
		
	}
    public static void main(String[] args) {
        double distanceMeters = 20;
        int hours = 3;
        int minutes = 13;
        int seconds = 20;

        // Calculating total time taken in hours
        double totalTimeHours = hours + (minutes / 60.0) + (seconds / 3600.0);

        // Calculating speed in meters per second
        double speedMetersPerSecond = distanceMeters / totalTimeHours;

        // Calculating speed in kilometers per hour
        double speedKilometersPerHour = (distanceMeters / 1000) / totalTimeHours;

        // Calculating speed in miles per hour
        double speedMilesPerHour = (distanceMeters / 1609) / totalTimeHours;

        // Displaying the speeds
        System.out.println("Speed:");
        System.out.println("Meters per second: " + speedMetersPerSecond);
        System.out.println("Kilometers per hour: " + speedKilometersPerHour);
        System.out.println("Miles per hour: " + speedMilesPerHour);

        
    }
}

