package com.demo2;

public class EMICalculator {
    public static void main(String[] args) {
        double carPrice = 850000; // Car price in rupees
        double downPayment = 150000; // Down payment in rupees
        int loanTermMonths = 48; // Loan term in months
        double annualInterestRate = 12; // Annual interest rate

        // Calculate loan amount
        double loanAmount = carPrice - downPayment;

        // Calculate monthly interest rate
        double monthlyInterestRate = (annualInterestRate / 100) / 12;

        // Calculate EMI
        double emi = (loanAmount * monthlyInterestRate) / (1 - Math.pow(1 + monthlyInterestRate, -loanTermMonths));

        // Round EMI to two decimal places
        emi = Math.round(emi * 100) / 100.0;

        System.out.println("EMI for the car loan: Rs. " + emi);
    }
}

