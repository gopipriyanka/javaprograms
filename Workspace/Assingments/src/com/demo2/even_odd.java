package com.demo2;

public class even_odd {
	public static int isEven(int number) {
        // Check if the number is negative or zero
        if (number <= 0) {
            return -1;
        }
        
        // Check if the number is even
        if (number % 2 == 0) {
            return 1;
        } else {
            return 0;
        }
    }
	
    public static void main(String[] args) {
        // Test the isEven() method
        System.out.println(isEven(22)); // Output: 1
        System.out.println(isEven(35)); // Output: 0
        System.out.println(isEven(-3)); // Output: -1
    }
    
}