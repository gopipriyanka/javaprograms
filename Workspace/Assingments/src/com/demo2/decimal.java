package com.demo2;


public class decimal {
	
	public static double dec(double d) {
		return Math.round(d * 100.0) / 100.0;
	}
    public static void main(String[] args) {
        System.out.println(dec(12.33333333333333));
    }
}