package day4;

public class leap_year {
	
	public static boolean isleap(int year){
		if ( (year % 100 != 0 && year % 4 == 0) || (year % 400 == 0)){
			return true;
		}
		else{
			return false;
		}
	}
	public static void main(String[] args) {
		System.out.println(isleap(2002));
		System.out.println(isleap(2000));
		System.out.println(isleap(1996));
		System.out.println(isleap(1700));

	}

}
