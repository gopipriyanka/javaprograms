package assignment;

import java.util.Scanner;

public class Billcalculator {

	public static void displayMenu() {
		System.out.println("Menu:");
		System.out.println("1.Apple Juice -Rs.70");
		System.out.println("2.Musk Melon Juice-Rs.60");
	}
	public static String getOrder() {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter your order:");
		int choice=sc.nextInt();
		
		switch(choice) {
		case 1:
			return "Apple Juice";
		case 2:
			return "Musk Melon";
		default:
			return "Invalid Choice";
		}
	}
	public static int calculateBill(String order) {
		switch(order) {
		case "Apple Juice":
			return 70;
		case "Musk Melon":
			return 60;
			default:
				return 0;
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		displayMenu();
		String order=getOrder();
		int totalBill=calculateBill(order);
		System.out.println("Total Bill:Rs."+totalBill);
	}

}
