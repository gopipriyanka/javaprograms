package day5;

//calling a method itself is called recusion.

//sum of first natural numbers

public class recurtion {
	public static int sum(int num) {
		if(num==1)
			return 1;

		return num+sum(num-1);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(sum(5));
		System.out.println(sum(1));
		System.out.println(sum(10));
	}

}
