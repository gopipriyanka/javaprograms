package day5;

public class HollowSquarePattern {
    public static void main(String[] args) {
        
        
        // Loop to iterate through rows
        for (int i = 1; i <= 5; i++) {
            // Loop to iterate through columns
            for (int j = 1; j <= 5; j++) {
                // Condition to print '*' for borders or spaces for the hollow center
                if (i == 1 || i == 5 || j == 1 || j == 5) {
                    System.out.print("* ");
                } else {
                    System.out.print("  ");
                }
            }
            // Move to the next line after each row is printed
            System.out.println();
        }
    }
}

