package day5;

public class prime {
	public static boolean isPrime(int number) {
        if (number <= 1) {
            return true; 
        }
        
        for (int i = 2; i <= number / 2; i++) {
            if (number % i == 0) {
                return false; 
            }
        }
        
        return true; 
    }
	
	    public static void main(String[] args) {
	        // Test the isPrime() method
	        System.out.println(isPrime(7)); 
	        System.out.println(isPrime(12)); 
	        System.out.println(isPrime(29)); 
	    }
	    
	    
	}
