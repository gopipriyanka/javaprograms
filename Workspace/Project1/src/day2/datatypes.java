package day2;

public class datatypes {
	public static void main(String[] args) {
		// Type casting
//		String s = "25";
//		byte b = Byte.parseByte(s); //string to Byte
//		short sh = Short.parseShort(s); //string to short
//		int  i = Integer.parseInt(s); //string to Int
//		long l = Long.parseLong(s); //string to Long
//		
//		System.out.println("String s:" +s);
//		System.out.println("Byte b:" +b);
//		System.out.println("Short sh:" +sh);
//		System.out.println("int i:" +i);
//		System.out.println("Long l:" +l);
//		
//		
//		//Lower to Higher
//				System.out.println("Lower to Higher:");
//				System.out.println("String s:" +s);
//				System.out.println("Byte b:" +b);
//				System.out.println("Short sh:" +sh);
//				System.out.println("int i:" +i);
//				System.out.println("Long l:" +l);
//				
//						
//				
//				l = 45;
//				i = (int) i;
//				sh = (short) sh;
//				b = (byte) b;
//				s = b + "";
//				
//				
//				//Higher to Lower
//				System.out.println("Higher to Lower:");
//				System.out.println("Long l:" +l);
//				System.out.println("int i:" +i);
//				System.out.println("Short sh:" +sh);
//				System.out.println("Byte b:" +b);
//				System.out.println("String s:" +s);
				
//		swapping two number
		
		int num1 = 10;
		int num2 = 20;
		int temp;
		
		System.out.println("before swapping:");
		System.out.println("num1=" +num1 +"\nnum2=" +num2);
		
		
		
		System.out.println("after swapping with temp:");
		temp = num1;
		num1 = num2;
		num2 = temp;
		System.out.println("num1=" +num1 +"\nnum2=" +num2);
		
		
		
		int number1 = 10;
		int number2 = 20;
		
		System.out.println("after swapping with arithematic operations");
		number1 += number2;
		number2 = number1 - number2;
		number1 -= number2;
		System.out.println("num1=" +number1 +"\nnum2=" +number2);

			
	}
}
