package day2;

public class operators {

	public static void main(String[] args) {
		//Arithmetic operator
//		int num1=10;
//		int num2=3;
//		
//		System.out.println("Sum="+(num1+num2));
//		System.out.println("Sub="+(num1-num2));
//		System.out.println("Mul="+(num1*num2));
//		System.out.println("Div="+(num1/num2));
//		System.out.println("Mod="+(num1%num2));
		
		//arithmetic assignment operator
		
//		int num1 = 10;
//		int num2 = 3;
//		
//		System.out.println("num1 = " + num1 + "\n num2 = " + num2 + "\n");
//		
//		num1 += num2;		
//		System.out.println("num1 = " + num1 + "\n num2 = " + num2 + "\n");
//		
//		num1 -= num2;
//		System.out.println("num1 = " + num1 + "\n num2 = " + num2 + "\n");
//		
//		num1 *= num2;
//		System.out.println("num1 = " + num1 + "\n num2 = " + num2 + "\n");
//		
//		
//		num1 /= num2;
//		System.out.println("num1 = " + num1 + "\n num2 = " + num2 + "\n");
		
//		uninary Operator
		
//		int num=10;
//		System.out.println("num="+num);
//		num++;
//		System.out.println("num="+num);
//		num--;
//		System.out.println("num="+num);
		
		int num1 = 10;
		int num2 = 0;
		System.out.println("before operations:");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
		
		//pre-increment
		num2 = ++ num1;
		System.out.println("applying pre-increment");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
		
		//post increment
		num2 = num1 ++;
		System.out.println("applying post-increment");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
		
		
		//pre-decrement
		num2 = -- num1;
		System.out.println("applying pre-decremnt");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
		
		//post increment
		num2 = num1 --;
		System.out.println("applying post-decremnt");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
	}
}