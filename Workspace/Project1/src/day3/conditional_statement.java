package day3;

public class conditional_statement {
	public static void main(String[] args) {
		//conditional constructs  1.if-else  2. simple if-else 3.multiple if-else 4.nested-if else
		int age = 22;

		//if-else
		if(age > 17){
			System.out.println("you are a major!");
		}else{
			System.out.println("you are a minor");
		}

		//even or odd

		int num=10;

		if(num %2 == 0){
			System.out.println("even");
		}else {
			System.out.println("odd");
		}


		//sum of the individual 2 digit numbers

		int n=23;
		int sum=0;
		int n1;

		while(n>0)
		{
			n1=n%10;
			sum=sum+n1;
			n=n/10;
		}
		System.out.println(sum);
		
	}
}